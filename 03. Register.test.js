I.goTo("https://www.tmra.io/user/signup/donor")

// Fill Register form
I.fill("First name", "Frisky")
I.fill("Last name", "Midiawan")
I.fill("Email", "friskymidiawan9@gmail.com")
I.fill("Password", "Supersecretpassword1!")
I.fill("Country", "Indonesia")
I.fill("State", "West Java")
I.fill("Address", "Cipaheut Selatan")

// Select phone code
I.click("#mui-component-select-phoneCode")
I.click("+62")

I.fill("Phone Number", "8818244002")
I.click("Register")

I.see("Registration success")